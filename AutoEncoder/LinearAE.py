"""
If the autoencoder uses only linear activations and the cost function is the Mean Squared Error(MSE), then it
can be shown that it ends up performing Principal Component Analysis

The following code builds a simple linear autoencoder to perform PAC on a 3D dataset, projecting to 2D:
"""